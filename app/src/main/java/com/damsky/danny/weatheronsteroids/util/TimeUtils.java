package com.damsky.danny.weatheronsteroids.util;

import android.support.annotation.NonNull;

/**
 * Util class for improving the String representation of time.
 *
 * @author Danny Damsky
 */
public final class TimeUtils {

    /**
     * Converts a 12-hour time String from the YAHOO Weather API to a 24-hour time String
     *
     * @param twelveHourTime A time String with the following format: "0-12:00-59 am/pm"
     * @return A 24-hour time String with the following format: "00-24:00-59"
     */
    public static String convertTwelveHourToTwentyFourHourTime(@NonNull String twelveHourTime) {
        final boolean isPm = twelveHourTime.contains("pm");
        String newString;
        int hours;
        if (isPm) {
            newString = twelveHourTime.replace("pm", "").trim();
            hours = 12;
        } else {
            newString = twelveHourTime.replace("am", "").trim();
            hours = 0;
        }
        final String[] splitTime = newString.split(":");
        hours += Integer.parseInt(splitTime[0]);
        final int minutes = Integer.parseInt(splitTime[1]);
        final StringBuilder builder = new StringBuilder();
        if (hours < 10) {
            builder.append(0);
        }
        builder.append(hours).append(':');
        if (minutes < 10) {
            builder.append(0);
        }
        builder.append(minutes);
        return builder.toString();
    }

    /**
     * Adds a '0' character at the beginning of the String if the first number is smaller than 10.
     *
     * @param twelveHourTime A time String with the following format: "0-12:00-59 am/pm"
     * @return A time String with the following format: "00-12:00-59 am/pm"
     */
    public static String fixYahooTwelveHourTimeString(@NonNull String twelveHourTime) {
        if (twelveHourTime.charAt(1) == ':') {
            return '0' + twelveHourTime;
        }
        return twelveHourTime;
    }
}
