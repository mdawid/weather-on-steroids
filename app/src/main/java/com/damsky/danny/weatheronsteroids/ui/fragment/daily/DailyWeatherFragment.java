package com.damsky.danny.weatheronsteroids.ui.fragment.daily;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.fragment.daily.adapter.DailyWeatherRecyclerAdapter;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.damsky.danny.weatheronsteroids.work.DbOpsWorker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * This fragment displays the daily weather forecast for a specific location.
 * The fragment is contained inside {@link com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.WeatherViewPagerFragment}.
 *
 * @author Danny Damsky
 * @see MainActivity
 * @see com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.WeatherViewPagerFragment
 */
public final class DailyWeatherFragment extends Fragment {

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private MainActivity activity;
    private Unbinder unbinder;
    private WeatherLocationViewModel weatherLocationViewModel;
    private OptimizedLocationItem locationItem;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(getContext())
                .inflate(R.layout.fragment_daily_weather, container, false);
        unbinder = ButterKnife.bind(this, layout);
        initProperties();
        return layout;
    }

    private void initProperties() {
        configureRefreshLayoutColorScheme();
        setDefaultRecyclerViewProperties();
        setObserver();
        setRefreshLayoutBehavior();
    }

    private void configureRefreshLayoutColorScheme() {
        refreshLayout.setColorSchemeResources(R.color.colorSecondary, R.color.colorTertiary,
                R.color.colorSecondaryLight, R.color.colorTertiaryLight, R.color.colorSecondaryDark,
                R.color.colorTertiaryDark);
    }

    private void setDefaultRecyclerViewProperties() {
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setRefreshLayoutBehavior() {
        refreshLayout.setOnRefreshListener(() ->
                DbOpsWorker.updateSinglePlace(activity, locationItem.getLocation()));
    }


    private void setObserver() {
        final String id = getArguments().getString(Constants.EXTRA_ID);
        weatherLocationViewModel.getOptimizedById(id).observe(this, locationItem -> {
            this.locationItem = locationItem;
            setRecyclerViewAdapter();
            this.refreshLayout.setRefreshing(false);
        });
    }

    private void setRecyclerViewAdapter() {
        final DailyWeatherRecyclerAdapter adapter = new DailyWeatherRecyclerAdapter(locationItem);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
