package com.damsky.danny.weatheronsteroids.data.weather.model;

import com.google.gson.annotations.SerializedName;

/**
 * This class is contained inside of {@link YahooChannel}.
 * It contains information about the sunset and sunrise times of the location.
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 * @see YahooResults
 * @see YahooChannel
 */
public final class YahooAstronomy {

    @SerializedName("sunset")
    private final String sunsetTime;
    @SerializedName("sunrise")
    private final String sunriseTime;

    public YahooAstronomy(String sunsetTime, String sunriseTime) {
        this.sunsetTime = sunsetTime;
        this.sunriseTime = sunriseTime;
    }

    public String getSunsetTime() {
        return sunsetTime;
    }

    public String getSunriseTime() {
        return sunriseTime;
    }

    @Override
    public String toString() {
        return "YahooAstronomy{" +
                "sunsetTime='" + sunsetTime + '\'' +
                ", sunriseTime='" + sunriseTime + '\'' +
                '}';
    }
}
