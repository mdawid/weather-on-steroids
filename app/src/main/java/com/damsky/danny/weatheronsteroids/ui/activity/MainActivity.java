package com.damsky.danny.weatheronsteroids.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.GlideApp;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.auth.AuthHelper;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedWeatherLocation;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.ui.activity.adapter.DrawerRecyclerAdapter;
import com.damsky.danny.weatheronsteroids.ui.fragment.dialog.alert.CustomizableDialogFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.dialog.alert.DialogFragmentBundle;
import com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.actions.ItemActionsFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.signout.SignOutFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.locations.LocationsWeatherFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.locations.adapter.LocationsRecyclerAdapter;
import com.damsky.danny.weatheronsteroids.ui.fragment.mapholder.MapHolderFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.InfoPagerFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.WeatherViewPagerFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.prefs.PreferencesFragment;
import com.damsky.danny.weatheronsteroids.ui.widget.MaterialSnackbar;
import com.damsky.danny.weatheronsteroids.ui.widget.SlidingBottomNavigationView;
import com.damsky.danny.weatheronsteroids.util.ColorUtils;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.damsky.danny.weatheronsteroids.util.IntentUtils;
import com.damsky.danny.weatheronsteroids.work.DbOpsWorker;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This activity controls all UI elements in the application (Except for WebViewActivity).
 * It is designed around the principle of having a single activity with multiple fragments.
 *
 * @author Danny Damsky
 */
public final class MainActivity extends AppCompatActivity implements LocationsRecyclerAdapter.Listener,
        ItemActionsFragment.Listener, CustomizableDialogFragment.TextChangeListener,
        CustomizableDialogFragment.SimpleActionListener, SignOutFragment.Listener,
        BottomNavigationView.OnNavigationItemSelectedListener, DrawerRecyclerAdapter.Listener,
        MapHolderFragment.Callback, LocationsWeatherFragment.Listener {

    @BindView(android.R.id.content)
    View content;
    @BindView(R.id.mainCoordinatorLayout)
    CoordinatorLayout mainCoordinatorLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.bottomNavView)
    SlidingBottomNavigationView bottomNavView;

    // Navigation Drawer is not present on tablets.
    @Nullable
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @Nullable
    @BindView(R.id.navigationDrawer)
    NavigationView navigationDrawer;
    @Nullable
    @BindView(R.id.navigationRecyclerView)
    RecyclerView navigationRecyclerView;
    @Nullable
    @BindView(R.id.emptyText)
    TextView emptyText;

    // Speed Dial is not present on phones.
    @Nullable
    @BindView(R.id.speedDial)
    SpeedDialView speedDial;

    private DrawerRecyclerAdapter adapter;
    private ActionBarDrawerToggle drawerToggle;
    private LiveData<List<OptimizedLocationItem>> itemsLiveData;
    private Observer<List<OptimizedLocationItem>> itemsObserver;
    private MenuItem googleSignInItem;
    private boolean googleSignInItemVisibility = true;
    private MenuItem searchItem;
    private boolean searchItemVisibility = false;
    private WeatherLocationViewModel weatherLocationViewModel;
    private boolean bottomSheetCollapsed = true;

    private String locationId;
    private String locationName;
    private String tag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        AppCompatDelegate.setDefaultNightMode(PreferencesHelper.getInstance().getNightMode());
        super.onCreate(savedInstanceState);
        restoreSavedInstanceState(savedInstanceState);
        if (hasLocationAccess()) {
            createActivity();
        } else {
            requestLocationPermission();
        }
    }

    private void restoreSavedInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.locationId = savedInstanceState.getString(Constants.EXTRA_ID);
            this.locationName = savedInstanceState.getString(Constants.EXTRA_LOCATION_NAME);
            this.tag = savedInstanceState.getString(Constants.EXTRA_TAG);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.EXTRA_ID, locationId);
        outState.putString(Constants.EXTRA_LOCATION_NAME, locationName);
        outState.putString(Constants.EXTRA_TAG, tag);
    }

    private boolean hasLocationAccess() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                Constants.REQUEST_CODE_LOCATION_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            createActivity();
        } else {
            FancyToast.makeText(getApplicationContext(), getString(R.string.error_permissions_not_granted), FancyToast.LENGTH_LONG,
                    FancyToast.INFO, false).show();
            finish();
        }
    }

    private void createActivity() {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initActivity();
    }

    private void initActivity() {
        initToolbar();
        initViewModel();
        if (drawerLayout != null) {
            initDrawerToggle();
            initLiveData();
            initRecyclerView();
        } else {
            initSpeedDial();
        }
        findFragments();
        bottomNavView.setOnNavigationItemSelectedListener(this);
    }

    private void initToolbar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    private void initDrawerToggle() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_nav_drawer, R.string.close_nav_drawer);
    }

    private void initViewModel() {
        weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
    }

    private void initLiveData() {
        itemsLiveData = weatherLocationViewModel.getAllAdded();
        setItemsObserver();
    }

    private void setItemsObserver() {
        itemsObserver = optimizedLocationItems -> {
            if (optimizedLocationItems != null && optimizedLocationItems.size() > 0) {
                showRecyclerView();
                adapter.submitList(optimizedLocationItems);
            } else {
                showEmptyText();
            }
        };
    }

    private void showEmptyText() {
        hideOneViewAndShowTheOther(navigationRecyclerView, emptyText);
    }

    private void showRecyclerView() {
        hideOneViewAndShowTheOther(emptyText, navigationRecyclerView);
    }

    private void hideOneViewAndShowTheOther(View viewToHide, View viewToShow) {
        toggleVisibility(viewToHide, View.VISIBLE, View.INVISIBLE);
        toggleVisibility(viewToShow, View.INVISIBLE, View.VISIBLE);
    }

    private void toggleVisibility(View v, int currentVisibility, int newVisibility) {
        if (v.getVisibility() == currentVisibility) {
            v.setVisibility(newVisibility);
        }
    }

    private void initRecyclerView() {
        adapter = new DrawerRecyclerAdapter();
        navigationRecyclerView.setItemAnimator(null);
        navigationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        navigationRecyclerView.setHasFixedSize(true);
        navigationRecyclerView.setAdapter(adapter);
    }

    private void initSpeedDial() {
        speedDialAddZoomToCurrentLocationItem();
        speedDialAddSearchLocationItem();
        speedDialAddNewLocationItem();
        speedDial.setExpansionMode(SpeedDialView.ExpansionMode.BOTTOM);
        speedDial.setOnActionSelectedListener(actionItem -> {
            switch (actionItem.getId()) {
                case R.id.action_zoom_to_current_location:
                    final MapHolderFragment mapHolderFragment = findMapHolderFragment();
                    mapHolderFragment.onMyLocationFabClicked();
                    break;
                case R.id.action_search_location:
                    IntentUtils.startPlacesAutocompleteForResult(this, content,
                            Constants.REQUEST_CODE_PLACES_SEARCH, PlaceAutocomplete.MODE_OVERLAY);
                    break;
                case R.id.action_add_new_location:
                    onFabClicked();
                    break;
            }
            return false;
        });
    }

    private void speedDialAddZoomToCurrentLocationItem() {
        speedDial.addActionItem(new SpeedDialActionItem.Builder(R.id.action_zoom_to_current_location, R.drawable.ic_my_location_white_24dp)
                .setFabBackgroundColor(ColorUtils.getColor(this, R.color.yellow_700))
                .setLabel(getString(R.string.zoom_to_current_location))
                .setLabelClickable(true)
                .create());
    }

    private void speedDialAddSearchLocationItem() {
        speedDial.addActionItem(new SpeedDialActionItem.Builder(R.id.action_search_location, R.drawable.ic_search_white_24dp)
                .setFabBackgroundColor(ColorUtils.getColor(this, R.color.colorTertiaryLight))
                .setLabel(getString(R.string.search_location))
                .setLabelClickable(true)
                .create());
    }

    private void speedDialAddNewLocationItem() {
        speedDial.addActionItem(new SpeedDialActionItem.Builder(R.id.action_add_new_location, R.drawable.ic_location_searching_white_24dp)
                .setFabBackgroundColor(ColorUtils.getColor(this, R.color.red_a700))
                .setLabel(getString(R.string.add_new_location))
                .setLabelClickable(true)
                .create());
    }

    private void findFragments() {
        if (tag == null) {
            showLocationsWeatherFragment();
        } else {
            findFragmentByTag();
        }
    }

    private void showLocationsWeatherFragment() {
        tag = LocationsWeatherFragment.TAG;
        LocationsWeatherFragment.show(getSupportFragmentManager(), R.id.fragmentContainer);
        bottomNavView.setSelectedItemId(R.id.action_weather);
        setDrawerState(false);
    }

    private void findFragmentByTag() {
        switch (tag) {
            case InfoPagerFragment.TAG:
                setDisplayForInfo();
                break;
            case LocationsWeatherFragment.TAG:
                showLocationsWeatherFragment();
                break;
            case WeatherViewPagerFragment.TAG:
                setDisplayForLocation(locationName);
                break;
            case MapHolderFragment.TAG:
                setDisplayForMap();
                break;
            default:
                showLocationsWeatherFragment();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_info:
                if (!tag.equals(InfoPagerFragment.TAG)) {
                    setDisplayForInfo();
                    InfoPagerFragment.show(getSupportFragmentManager(), R.id.fragmentContainer);
                }
                break;
            case R.id.action_weather:
                if (!tag.equals(LocationsWeatherFragment.TAG) && !tag.equals(WeatherViewPagerFragment.TAG)) {
                    setDisplayForMain();
                    LocationsWeatherFragment.show(getSupportFragmentManager(), R.id.fragmentContainer);
                }
                break;
            case R.id.action_map:
                if (!tag.equals(MapHolderFragment.TAG)) {
                    setDisplayForMap();
                    MapHolderFragment.show(getSupportFragmentManager(), R.id.fragmentContainer);
                }
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        final MapHolderFragment mapHolderFragment = findMapHolderFragment();
        if (mapHolderFragment != null && !bottomSheetCollapsed) {
            mapHolderFragment.collapseBottomSheet();
            onBottomSheetCollapsed();
        } else {
            if (tag != null && tag.equals(WeatherViewPagerFragment.TAG)) {
                bottomNavView.show();
                setDisplayForMain();
            }
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        initGoogleSignInItem(menu);
        initSearchItem(menu);
        if (AuthHelper.isLoggedIn()) {
            loadUserImageIntoMenuItem();
        }
        return true;
    }

    private void initGoogleSignInItem(Menu menu) {
        googleSignInItem = menu.findItem(R.id.action_log_in);
        googleSignInItem.setVisible(googleSignInItemVisibility);
    }

    private void initSearchItem(Menu menu) {
        searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(searchItemVisibility);
    }

    private void loadUserImageIntoMenuItem() {
        GlideApp.with(this).load(AuthHelper.getPhotoUrl()).circleCrop().into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                googleSignInItem.setIcon(resource);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_log_in:
                onLoginMenuItemPressed();
                break;
            case R.id.action_search:
                IntentUtils.startPlacesAutocompleteForResult(this, content,
                        Constants.REQUEST_CODE_PLACES_SEARCH, PlaceAutocomplete.MODE_OVERLAY);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void onLoginMenuItemPressed() {
        if (!AuthHelper.isLoggedIn()) {
            AuthHelper.startGoogleLoginIntent(this, Constants.REQUEST_CODE_LOG_IN);
        } else {
            SignOutFragment.show(getSupportFragmentManager());
        }
    }

    @Override
    public void onLogOutPressed() {
        AuthHelper.logOut();
        googleSignInItem.setIcon(R.drawable.ic_google_primary_black_24dp);
    }

    @Override
    public void onFabClicked() {
        IntentUtils.startPlacePickerForResult(this, content, Constants.REQUEST_CODE_PLACE_PICKER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.REQUEST_CODE_PLACE_PICKER:
                handleResultFromPlacePicker(resultCode, data);
                break;
            case Constants.REQUEST_CODE_LOG_IN:
                handleResultFromLogIn();
                break;
            case Constants.REQUEST_CODE_PLACES_SEARCH:
                handlePlacesSearchResult(resultCode, data);
                break;
            default:
                logErrorFromResult("onActivityResult", data);
                break;
        }
    }

    private void handleResultFromPlacePicker(int resultCode, @Nullable Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                insertNewPlace(data);
                break;
            case PlacePicker.RESULT_ERROR:
                logErrorWithStatus(data);
                break;
            case RESULT_CANCELED:
                Log.d("RESULT_CANCELED", "Result was canceled from PlacePicker");
                break;
            default:
                logErrorFromResult("handleResultFromPlacePicker", data);
                break;
        }
    }

    private void insertNewPlace(Intent data) {
        final Place place = PlacePicker.getPlace(this, data);
        final LiveData<WeatherLocation> liveData = weatherLocationViewModel.getById(place.getId());
        liveData.observe(this, location -> {
            if (location != null) {
                weatherLocationViewModel.restore(place.getId());
            } else {
                DbOpsWorker.insertNewPlace(this, place);
            }
            liveData.removeObservers(this);
        });
    }

    private void logErrorWithStatus(Intent data) {
        final Status status = PlacePicker.getStatus(this, data);
        Crashlytics.log(Log.ERROR, "RESULT_ERROR", "Status Code: " + status.getStatusCode()
                + "\n" + "Status Message: " + status.getStatusMessage());
    }

    private void handleResultFromLogIn() {
        if (AuthHelper.isLoggedIn()) {
            showWelcomeMessage();
            loadUserImageIntoMenuItem();
            DbOpsWorker.syncWithFirebase(this);
        }
    }

    private void showWelcomeMessage() {
        MaterialSnackbar.make(content,
                getString(R.string.welcome) + ", " + AuthHelper.getDisplayName(),
                Snackbar.LENGTH_LONG).show();
    }

    private void logErrorFromResult(@NonNull String functionName, @Nullable Intent data) {
        if (data == null) {
            Crashlytics.log(Log.ERROR, "ERROR_UNKNOWN", "Error in " + functionName);
        } else {
            Crashlytics.log(Log.ERROR, "ERROR_UNKNOWN", "Error in " + functionName + "\ndata=" + data.toString());
        }
    }

    private void handlePlacesSearchResult(int resultCode, @Nullable Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                zoomMapIntoLocationFromSearch(data);
                break;
            case PlaceAutocomplete.RESULT_ERROR:
                logErrorWithStatus(data);
                break;
            case RESULT_CANCELED:
                Log.d("RESULT_CANCELED", "result was canceled from Search Autocomplete");
                break;
            default:
                logErrorFromResult("handlePlacesSearchResult", data);
                break;
        }
    }

    private void zoomMapIntoLocationFromSearch(@NonNull Intent data) {
        final Place place = PlaceAutocomplete.getPlace(this, data);
        final MapHolderFragment mapHolderFragment = findMapHolderFragment();
        if (mapHolderFragment != null) {
            mapHolderFragment.zoomTo(place);
        }
    }

    @Override
    public void onItemLongClicked(@NonNull OptimizedLocationItem location) {
        this.locationId = location.getLocation().getId();
        ItemActionsFragment.show(getSupportFragmentManager());
    }

    @Override
    public void onEditPressed() {
        final LiveData<WeatherLocation> liveLocation = weatherLocationViewModel.getById(locationId);
        liveLocation.observe(this, location -> {
            DialogFragmentBundle bundle = buildBundleForRenaming(location);
            CustomizableDialogFragment.show(getSupportFragmentManager(), bundle);
            liveLocation.removeObservers(this);
        });
    }

    private DialogFragmentBundle buildBundleForRenaming(@NonNull WeatherLocation location) {
        return new DialogFragmentBundle.Builder()
                .setTitle(getString(R.string.action_rename_location))
                .setHint(getString(R.string.hint_location_name))
                .setText(location.getLocationName())
                .setPositiveText(getString(R.string.rename))
                .setNegativeText(getString(R.string.cancel))
                .build();
    }

    @Override
    public void onEditableDialogComplete(@NonNull String text) {
        final LiveData<WeatherLocation> liveLocation = weatherLocationViewModel.getById(locationId);
        liveLocation.observe(this, location -> {
            location.setLocationName(text);
            weatherLocationViewModel.update(location);
            liveLocation.removeObservers(this);
        });
    }

    @Override
    public void onDeletePressed() {
        final LiveData<WeatherLocation> liveLocation = weatherLocationViewModel.getById(locationId);
        liveLocation.observe(this, location -> {
            final DialogFragmentBundle bundle = buildBundleForDeleting();
            CustomizableDialogFragment.show(getSupportFragmentManager(), bundle);
            liveLocation.removeObservers(this);
        });
    }

    private DialogFragmentBundle buildBundleForDeleting() {
        return new DialogFragmentBundle.Builder()
                .setTitle(getString(R.string.delete_location_title))
                .setMessage(getString(R.string.delete_location_message))
                .setPositiveText(getString(R.string.delete))
                .setNegativeText(getString(R.string.cancel))
                .build();
    }

    @Override
    public void onDialogComplete(@Nullable String fragmentTag) {
        if (fragmentTag != null && fragmentTag.equals(PreferencesFragment.TAG)) {
            deleteAllLocations();
        } else {
            setupLocationObserverById();
        }
    }

    private void deleteAllLocations() {
        weatherLocationViewModel.deleteAll();
        MaterialSnackbar.make(content, R.string.delete_all_success, Snackbar.LENGTH_SHORT).show();
    }

    private void setupLocationObserverById() {
        final LiveData<WeatherLocation> liveLocation = weatherLocationViewModel.getById(locationId);
        liveLocation.observe(this, location -> {
            weatherLocationViewModel.delete(location);
            liveLocation.removeObservers(this);
        });
    }

    @Override
    public void onItemClicked(@NonNull OptimizedLocationItem locationItem) {
        final OptimizedWeatherLocation location = locationItem.getLocation();
        setDisplayForLocation(location.getLocationName());
        WeatherViewPagerFragment.show(getSupportFragmentManager(),
                R.id.fragmentContainer, drawerLayout == null, location.getId());
        this.locationName = location.getLocationName();

        final MapHolderFragment mapHolderFragment = findMapHolderFragment();
        if (mapHolderFragment != null) {
            mapHolderFragment.zoomTo(locationItem);
        }
    }

    private void setDisplayForLocation(@NonNull String locationName) {
        tag = WeatherViewPagerFragment.TAG;
        setDrawerState(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTitle.setText(locationName);
        bottomNavView.hide();
        setGoogleSignInItemVisible(false);
    }

    private void setDisplayForMain() {
        tag = LocationsWeatherFragment.TAG;
        setDrawerState(false);
        final boolean shouldDisplayMain = drawerLayout == null && !bottomSheetCollapsed;
        getSupportActionBar().setDisplayHomeAsUpEnabled(shouldDisplayMain);
        if (!shouldDisplayMain) {
            toolbarTitle.setText(R.string.app_name);
        }
        setGoogleSignInItemVisible(true);
        setSearchItemVisible(false);
        if (itemsLiveData != null) {
            itemsLiveData.removeObserver(itemsObserver);
        }
    }

    private void setDisplayForInfo() {
        tag = InfoPagerFragment.TAG;
        setDrawerState(false);
        final boolean shouldDisplayInfo = drawerLayout == null && !bottomSheetCollapsed;
        getSupportActionBar().setDisplayHomeAsUpEnabled(shouldDisplayInfo);
        if (!shouldDisplayInfo) {
            toolbarTitle.setText(R.string.action_info);
        }
        setGoogleSignInItemVisible(false);
        setSearchItemVisible(false);
        if (itemsLiveData != null) {
            itemsLiveData.removeObserver(itemsObserver);
        }
    }

    private void setDisplayForMap() {
        if (drawerLayout != null) {
            tag = MapHolderFragment.TAG;
            setDrawerState(true);
            toolbarTitle.setText(R.string.action_map);
            setGoogleSignInItemVisible(false);
            setSearchItemVisible(true);
            itemsLiveData.observe(this, itemsObserver);
        }
    }

    private void setDrawerState(boolean isEnabled) {
        if (drawerLayout != null) {
            if (isEnabled) {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            } else {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                drawerToggle.setToolbarNavigationClickListener(view -> onBackPressed());
            }
            drawerToggle.setDrawerIndicatorEnabled(isEnabled);
            drawerToggle.syncState();
        }
    }

    private void setGoogleSignInItemVisible(boolean visibility) {
        if (googleSignInItem != null) {
            googleSignInItem.setVisible(visibility);
        } else {
            this.googleSignInItemVisibility = visibility;
        }
    }

    private void setSearchItemVisible(boolean visibility) {
        if (searchItem != null) {
            searchItem.setVisible(visibility);
        } else {
            this.searchItemVisibility = visibility;
        }
    }

    @Override
    public void onNavigationDrawerItemClicked(@NonNull OptimizedLocationItem location) {
        if (tag.equals(MapHolderFragment.TAG)) {
            final MapHolderFragment mapHolderFragment = findMapHolderFragment();
            mapHolderFragment.zoomTo(location);
            drawerLayout.closeDrawers();
        }
    }

    @Override
    public void onMapZoomTrackingStarted() {
        toolbarTitle.setText(R.string.zoom);
        if (speedDial != null) {
            speedDial.hide();
        }
    }

    @Override
    public void onMapZoomProgressChanged(float progress) {
        // Lint is suppressed because String.format works just fine with all locales in this example.
        @SuppressLint("DefaultLocale") final String zoomString = String.format("%s: %.2f",
                getString(R.string.zoom), progress);
        toolbarTitle.setText(zoomString);
    }

    @Override
    public void onMapZoomTrackingEnded() {
        toolbarTitle.setText(R.string.action_map);
        if (speedDial != null) {
            speedDial.show();
        }
    }

    @Override
    public void onBottomSheetExpanded(@NonNull String locationName) {
        bottomSheetCollapsed = false;
        setDrawerState(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSearchItemVisible(false);
        toolbarTitle.setText(locationName);
        if (speedDial != null) {
            speedDial.hide();
        }
    }

    @Override
    public void onBottomSheetCollapsed() {
        if (!bottomSheetCollapsed) {
            bottomSheetCollapsed = true;
            if (drawerLayout != null) {
                onBottomSheetCollapsedPhone();
            } else {
                onBottomSheetCollapsedTablet();
            }
        }
    }

    private void onBottomSheetCollapsedPhone() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setDrawerState(true);
        setSearchItemVisible(true);
        toolbarTitle.setText(R.string.action_map);
    }

    private void onBottomSheetCollapsedTablet() {
        final boolean viewPagerIsOpen = tag != null && tag.equals(WeatherViewPagerFragment.TAG);
        getSupportActionBar().setDisplayHomeAsUpEnabled(viewPagerIsOpen);
        if (!viewPagerIsOpen) {
            if (tag != null && tag.equals(LocationsWeatherFragment.TAG)) {
                toolbarTitle.setText(R.string.app_name);
            } else if (tag != null && tag.equals(InfoPagerFragment.TAG)) {
                toolbarTitle.setText(R.string.action_info);
            }
        } else {
            toolbarTitle.setText(locationName);
        }
        speedDial.show();
    }

    @Nullable
    private MapHolderFragment findMapHolderFragment() {
        return (MapHolderFragment) getSupportFragmentManager()
                .findFragmentByTag(MapHolderFragment.TAG);
    }
}
