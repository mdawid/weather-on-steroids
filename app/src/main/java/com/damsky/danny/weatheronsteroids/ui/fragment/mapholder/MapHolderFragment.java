package com.damsky.danny.weatheronsteroids.ui.fragment.mapholder;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhoto;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedWeatherLocation;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.fragment.mainmap.MapFragment;
import com.damsky.danny.weatheronsteroids.ui.widget.DraggableFloatingActionButton;
import com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet.PlacesBottomSheetView;
import com.damsky.danny.weatheronsteroids.work.DbOpsWorker;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import java.util.UUID;

import androidx.work.WorkManager;
import androidx.work.WorkStatus;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * Holds the {@link MapFragment} class as well as some extra UI elements to help navigate through
 * the map.
 * <br /><br />
 * When using this class make sure to implement {@link Callback}.
 *
 * @author Danny Damsky
 */
public final class MapHolderFragment extends Fragment
        implements DraggableFloatingActionButton.OnProgressChangeListener,
        MapFragment.Listener, PlacesBottomSheetView.Callback {

    public static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.mapholder.MapHolderFragment.TAG";

    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayoutId) {
        if (fragmentManager.findFragmentByTag(TAG) == null) {
            final MapHolderFragment fragment = new MapHolderFragment();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in_short, R.anim.fade_out_short);
            transaction.replace(parentLayoutId, fragment, TAG);
            transaction.commit();
        }
    }

    @BindView(R.id.zoomFab)
    DraggableFloatingActionButton zoomFab;
    @BindView(R.id.zoomLayout)
    ConstraintLayout zoomLayout;
    @BindView(R.id.placesBottomSheetView)
    PlacesBottomSheetView placesBottomSheetView;

    // myLocationFab only exists on phones.
    @Nullable
    @BindView(R.id.myLocationFab)
    FloatingActionButton myLocationFab;

    private MainActivity context;
    private Callback callback;
    private Unbinder unbinder;
    private WeatherLocationViewModel weatherLocationViewModel;

    private final Observer<PagedList<LocationPhoto>> photosObserver = photos ->
            placesBottomSheetView.submitList(photos);
    private LiveData<PagedList<LocationPhoto>> photosLiveData;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (MainActivity) context;
        this.callback = (Callback) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, layout);
        MapFragment.show(getChildFragmentManager(), R.id.mapLayout);
        zoomFab.setOnProgressChangeListener(this);
        initPlacesBottomSheetView();
        return layout;
    }

    private void initPlacesBottomSheetView() {
        placesBottomSheetView.from(placesBottomSheetView);
        if (myLocationFab != null) {
            placesBottomSheetView.setFabs(myLocationFab, zoomFab);
        } else {
            placesBottomSheetView.setFabs(zoomFab);
        }
        placesBottomSheetView.setCallback(this);
    }

    @Optional
    @OnClick(R.id.myLocationFab)
    public void onMyLocationFabClicked() {
        final MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentByTag(MapFragment.TAG);
        if (mapFragment != null) {
            mapFragment.zoomToCurrentLocation();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onTrackingStarted() {
        callback.onMapZoomTrackingStarted();
        if (myLocationFab != null) {
            myLocationFab.hide();
        }
    }

    @Override
    public void onProgressChanged(float progress) {
        callback.onMapZoomProgressChanged(progress);
        final MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentByTag(MapFragment.TAG);
        if (mapFragment != null) {
            mapFragment.zoomTo(progress);
        }
    }

    @Override
    public void onTrackingEnded() {
        callback.onMapZoomTrackingEnded();
        if (myLocationFab != null) {
            myLocationFab.show();
        }
    }

    public void zoomTo(@NonNull OptimizedLocationItem optimizedLocationItem) {
        final OptimizedWeatherLocation location = optimizedLocationItem.getLocation();
        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        zoomTo(latLng);
        placesBottomSheetView.show(location.getLocationName());
        placesBottomSheetView.setLocation(optimizedLocationItem);
    }

    public void zoomTo(@NonNull Place place) {
        zoomTo(place.getLatLng());
        final String locationName = place.getName().toString();
        placesBottomSheetView.show(locationName);
        constructNewItem(place.getId(), locationName, place.getLatLng());
    }

    private void zoomTo(@NonNull LatLng latLng) {
        final MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentByTag(MapFragment.TAG);
        if (mapFragment != null) {
            mapFragment.zoomToPlace(latLng);
        }
    }

    @Override
    public void onMapClicked(@Nullable String locationId, @Nullable String locationName, @Nullable LatLng latLng, boolean shouldDismiss) {
        if (shouldDismiss && placesBottomSheetView.isVisible()) {
            placesBottomSheetView.hide();
        } else if (locationId != null && locationName != null && latLng != null) {
            inflateSheet(locationId, locationName, latLng);
        }
    }

    private void inflateSheet(@NonNull String locationId, @NonNull String locationName, @NonNull LatLng latLng) {
        placesBottomSheetView.show(locationName);
        observeExistingItem(locationId, locationName, latLng);
    }

    private void observeExistingItem(@NonNull String locationId, @NonNull String locationName, @NonNull LatLng latLng) {
        final LiveData<OptimizedLocationItem> liveData = weatherLocationViewModel.getOptimizedByLocation(latLng);
        final Observer<OptimizedLocationItem> observer = optimizedLocationItem -> {
            if (optimizedLocationItem != null) {
                placesBottomSheetView.setLocation(optimizedLocationItem);
            } else {
                constructNewItem(locationId, locationName, latLng);
            }
            liveData.removeObservers(this);
        };
        liveData.observe(this, observer);
    }

    private void constructNewItem(@NonNull String locationId, @NonNull String locationName, @NonNull LatLng latLng) {
        final LiveData<OptimizedLocationItem> liveData = weatherLocationViewModel.getOptimizedById(locationId);
        liveData.observe(this, optimizedLocationItem -> {
            if (optimizedLocationItem != null) {
                placesBottomSheetView.setLocation(optimizedLocationItem);
            } else {
                final WeatherLocation weatherLocation = buildWeatherLocation(locationId, locationName, latLng);
                final UUID workId = DbOpsWorker.insertNewPlace(weatherLocation);
                observeWorkStatus(locationId, workId);
            }
        });
    }

    private void observeWorkStatus(@NonNull String locationId, @NonNull UUID workId) {
        final LiveData<WorkStatus> liveData = WorkManager.getInstance().getStatusById(workId);
        liveData.observe(context, workStatus -> {
            if (workStatus != null) {
                switch (workStatus.getState()) {
                    case FAILED:
                        placesBottomSheetView.notifyNoWeatherDataAvailable();
                        Log.d("WORK_STATUS", "setStatusListener: FAILED");
                        break;
                    case BLOCKED:
                        placesBottomSheetView.notifyNoWeatherDataAvailable();
                        Log.d("WORK_STATUS", "setStatusListener: BLOCKED");
                        break;
                    case RUNNING:
                        Log.d("WORK_STATUS", "setStatusListener: RUNNING");
                        break;
                    case ENQUEUED:
                        Log.d("WORK_STATUS", "setStatusListener: ENQUEUED");
                        break;
                    case CANCELLED:
                        Log.d("WORK_STATUS", "setStatusListener: CANCELLED");
                        break;
                    case SUCCEEDED:
                        onWeatherLocationBuilt(locationId);
                        Log.d("WORK_STATUS", "setStatusListener: SUCCEEDED");
                        break;
                }
            }
        });
    }

    private void onWeatherLocationBuilt(@NonNull String locationId) {
        final LiveData<OptimizedLocationItem> liveData = weatherLocationViewModel.getOptimizedById(locationId);
        liveData.observe(this, optimizedLocationItem -> {
            if (optimizedLocationItem != null) {
                placesBottomSheetView.setLocation(optimizedLocationItem);
            }
        });
    }

    private WeatherLocation buildWeatherLocation(@NonNull String locationId, @NonNull String locationName, @NonNull LatLng latLng) {
        return new WeatherLocation.Builder()
                .setId(locationId)
                .setLocationName(locationName)
                .setLatitude(latLng.latitude)
                .setLongitude(latLng.longitude)
                .setAdded(false)
                .build();
    }

    @Override
    public void onDeletePressed(@NonNull OptimizedLocationItem item) {
        weatherLocationViewModel.delete(item.getLocation().getId());
    }

    @Override
    public void onAddPressed(@NonNull OptimizedLocationItem item) {
        weatherLocationViewModel.restore(item.getLocation().getId());
    }

    @Override
    public void onBottomSheetExpanded(@NonNull String locationName) {
        callback.onBottomSheetExpanded(locationName);
    }

    @Override
    public void onBottomSheetCollapsed() {
        destroyPhotosLiveData();
        callback.onBottomSheetCollapsed();
    }

    @Override
    public void onBottomSheetPreparingMaximizedLayout(@NonNull String id) {
        destroyPhotosLiveData();
        photosLiveData = weatherLocationViewModel.getPagedLocationPhotos(id);
        photosLiveData.observe(this, photosObserver);
    }

    private void destroyPhotosLiveData() {
        if (photosLiveData != null) {
            photosLiveData.removeObserver(photosObserver);
            photosLiveData = null;
        }
    }

    public void collapseBottomSheet() {
        placesBottomSheetView.collapse();
    }

    public interface Callback {
        void onMapZoomTrackingStarted();

        void onMapZoomProgressChanged(float progress);

        void onMapZoomTrackingEnded();

        void onBottomSheetExpanded(@NonNull String locationName);

        void onBottomSheetCollapsed();
    }
}
