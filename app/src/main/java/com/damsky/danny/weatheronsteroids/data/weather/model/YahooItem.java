package com.damsky.danny.weatheronsteroids.data.weather.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

/**
 * This class is contained inside of {@link YahooChannel}.
 * It contains the most important information about the location.
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 * @see YahooResults
 * @see YahooChannel
 */
public final class YahooItem {

    @SerializedName("lat")
    private final double latitude;
    @SerializedName("long")
    private final double longitude;
    private final YahooCondition condition;
    private final YahooForecast[] forecast;

    public YahooItem(double latitude, double longitude, YahooCondition condition, YahooForecast[] forecast) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.condition = condition;
        this.forecast = forecast;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public YahooCondition getCondition() {
        return condition;
    }

    public YahooForecast[] getForecast() {
        return forecast;
    }

    @Override
    public String toString() {
        return "YahooItem{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", condition=" + condition +
                ", forecast=" + Arrays.toString(forecast) +
                '}';
    }
}
