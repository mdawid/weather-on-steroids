package com.damsky.danny.weatheronsteroids.data.db;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.content.Context;
import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhoto;
import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhotoDao;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocationDao;
import com.damsky.danny.weatheronsteroids.data.firestore.FirestoreHelper;
import com.google.firebase.firestore.CollectionReference;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * The repository class for the database.
 * It includes database select, insert and update operations (and these operations are also
 * applied to the Firebase Firestore database if the user is logged in).
 * This class is a singleton, it should only have once instance for the entire application.
 *
 * @author Danny Damsky
 */
public final class WeatherLocationRepository {

    private static WeatherLocationRepository INSTANCE;

    public static WeatherLocationRepository getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            synchronized (WeatherLocationRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = new WeatherLocationRepository(context);
                }
            }
        }
        return INSTANCE;
    }

    private final WeatherLocationDao weatherLocationDao;
    private final LiveData<List<WeatherLocation>> allWeatherLocations;
    private final LocationPhotoDao locationPhotoDao;

    private WeatherLocationRepository(@NonNull Context context) {
        final WeatherLocationRoomDatabase db = WeatherLocationRoomDatabase.getInstance(context);
        weatherLocationDao = db.weatherLocationDao();
        allWeatherLocations = weatherLocationDao.getAllAdded();
        locationPhotoDao = db.locationPhotoDao();
    }

    /**
     * @return {@link LiveData} of all {@link WeatherLocation} objects where isAdded = true
     */
    public LiveData<List<WeatherLocation>> getAllAdded() {
        return allWeatherLocations;
    }

    /**
     * Do not run this function on the main thread! It will cause the application to crash
     * since RoomDB doesn't allow DB operations on the main thread.
     *
     * @return all {@link WeatherLocation} objects in the database.
     */
    public List<WeatherLocation> getAllBlocking() {
        return weatherLocationDao.getAllBlocking();
    }

    /**
     * @param id the id of the location.
     * @return a {@link LiveData} object of the location.
     */
    public LiveData<WeatherLocation> getById(@NonNull String id) {
        return weatherLocationDao.getById(id);
    }

    /**
     * Do not run this function on the main thread! It will cause the application to crash
     * since RoomDB doesn't allow DB operations on the main thread.
     *
     * @param id the id of the location.
     * @return a {@link LiveData} object of the location.
     */
    public WeatherLocation getByIdBlocking(@NonNull String id) {
        return weatherLocationDao.getByIdBlocking(id);
    }

    /**
     * @param latitude  the latitude of the location
     * @param longitude the longitude of the location
     * @return {@link LiveData} object of the location.
     */
    public LiveData<WeatherLocation> getByLocation(double latitude, double longitude) {
        return weatherLocationDao.getByLocation(latitude, longitude);
    }

    /**
     * Inserts the location to the database asynchronously.
     * If the user is logged in, it also inserts the location to Firebase Firestore.
     *
     * @param location the location to insert to the database.
     */
    @SuppressLint("CheckResult")
    public void insert(@NonNull WeatherLocation location) {
        asyncNoReturn(emitter -> weatherLocationDao.insert(location));
        insertOrUpdateInFirebase(location);
    }

    private void insertOrUpdateInFirebase(@NonNull WeatherLocation location) {
        final CollectionReference userCollection = FirestoreHelper.getUserCollection();
        if (userCollection != null) {
            FirestoreHelper.getLocationDocument(userCollection, location).set(location);
        }
    }

    /**
     * Inserts all local {@link WeatherLocation} objects into the Firebase Firestore database
     * if the user is logged in.
     */
    @SuppressLint("CheckResult")
    public void insertOrUpdateAllInFirebase() {
        Observable.defer(() -> Observable.just(weatherLocationDao.getAllBlocking()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::insertOrUpdateAllInFirebase);
    }

    private void insertOrUpdateAllInFirebase(List<WeatherLocation> locations) {
        final CollectionReference userCollection = FirestoreHelper.getUserCollection();
        if (userCollection != null) {
            final int size = locations.size();
            for (int i = 0; i < size; i++) {
                final WeatherLocation location = locations.get(i);
                FirestoreHelper.getLocationDocument(userCollection, location).set(location);
            }
        }
    }

    /**
     * Updates the location in the database asynchronously.
     * If the user is logged in, it also updates the location in Firebase Firestore.
     *
     * @param location the location to update in the database.
     */
    public void update(@NonNull WeatherLocation location) {
        asyncNoReturn(emitter -> weatherLocationDao.update(location));
        insertOrUpdateInFirebase(location);
    }

    /**
     * Updates the locations in the database asynchronously.
     * If the user is logged in, it also updates the locations in Firebase Firestore.
     *
     * @param locations the locations to update in the database.
     */
    public void updateAll(@NonNull List<WeatherLocation> locations) {
        asyncNoReturn(emitter -> weatherLocationDao.updateAll(locations));
        insertOrUpdateAllInFirebase();
    }

    /**
     * Restores the location by setting its isAdded property to true.
     * If the user is logged in, it also restores the locations in Firebase Firestore.
     *
     * @param id the ID of the location to restore.
     */
    @SuppressLint("CheckResult")
    public void restoreLocation(@NonNull String id) {
        Observable.just(id).map(s -> {
            weatherLocationDao.restore(s);
            return weatherLocationDao.getByIdBlocking(s);
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::deleteFromFirebase);
    }

    /**
     * Removes the location from user visibility by setting its isAdded property to false.
     * If the user is logged in, it also removes the locations in Firebase Firestore.
     *
     * @param id the ID of the location to remove.
     */
    @SuppressLint("CheckResult")
    public void deleteLocation(@NonNull String id) {
        Observable.just(id).map(s -> {
            final WeatherLocation weatherLocation = weatherLocationDao.getByIdBlocking(s);
            weatherLocationDao.delete(weatherLocation.getId());
            return weatherLocation;
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::deleteFromFirebase);
    }

    /**
     * Removes the location from user visibility by setting its isAdded property to false.
     * If the user is logged in, it also removes the locations in Firebase Firestore.
     *
     * @param location the location to remove.
     */
    public void delete(@NonNull WeatherLocation location) {
        asyncNoReturn(emitter -> weatherLocationDao.delete(location.getId()));
        deleteFromFirebase(location);
    }

    private void deleteFromFirebase(@NonNull WeatherLocation location) {
        location.setAdded(false);
        insertOrUpdateInFirebase(location);
    }

    /**
     * Removes all locations from user visibility by setting their isAdded property to false.
     * If the user is logged in, the function also removes the locations in Firebase Firestore.
     */
    @SuppressLint("CheckResult")
    public void deleteAll() {
        Observable.create(emitter -> weatherLocationDao.deleteAll())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> deleteAllFromFirebase());
    }

    private void deleteAllFromFirebase() {
        final CollectionReference userCollection = FirestoreHelper.getUserCollection();
        if (userCollection != null) {
            asyncNoReturn(emitter -> {
                final List<WeatherLocation> locations = weatherLocationDao.getAllBlocking();
                final int size = locations.size();
                for (int i = 0; i < size; i++) {
                    WeatherLocation location = locations.get(i);
                    userCollection.document(location.getId()).set(location);
                }
            });
        }
    }

    /**
     * Inserts the photo to the database. Photos are only inserted to the local database and never
     * to Firebase Firestore.
     *
     * @param locationPhoto the photo to insert to the local database.
     */
    public void insert(@NonNull LocationPhoto locationPhoto) {
        asyncNoReturn(emitter -> locationPhotoDao.insert(locationPhoto));
    }

    /**
     * Inserts {@link WeatherLocation} objects to the database.
     * Note that this function only inserts the locations locally and by default it runs
     * on the main thread, which, if not handled correctly will result in a crash.
     *
     * @param locations an array of locations to insert
     */
    public void insertAllBlocking(@NonNull WeatherLocation... locations) {
        weatherLocationDao.insertAll(locations);
    }

    /**
     * @param placeId the ID of the place to get the photos from.
     * @return a {@link DataSource.Factory} object of the location's photos.
     */
    @NonNull
    public DataSource.Factory<Integer, LocationPhoto> getLocationPhotosDataSource(@NonNull String placeId) {
        return locationPhotoDao.getLocationPhotosDataSource(placeId);
    }

    private void asyncNoReturn(@NonNull ObservableOnSubscribe<Object> emitter) {
        Observable.create(emitter)
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

}
