package com.damsky.danny.weatheronsteroids.data.optimization;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.damsky.danny.weatheronsteroids.data.weather.config.WeatherConditionCode;
import com.damsky.danny.weatheronsteroids.data.weather.config.YahooDay;
import com.damsky.danny.weatheronsteroids.data.weather.model.YahooForecast;

import java.util.Objects;

/**
 * This class is contained inside {@link OptimizedLocationItem} as an array.
 * This class contains weather information for a certain day.
 *
 * @author Danny Damsky
 * @see OptimizedLocationItem
 * @see YahooForecast
 * @see WeatherConditionCode
 * @see YahooDay
 */
public final class OptimizedForecastItem {

    @StringRes
    private final int conditionId;
    @DrawableRes
    private final int conditionImageId;
    private final int dayTemp;
    private final int nightTemp;
    @StringRes
    private final int dayId;

    OptimizedForecastItem(@NonNull YahooForecast forecast) {
        if (forecast.getConditionCode() == WeatherConditionCode.NOT_AVAILABLE.getCode()) {
            this.conditionId = WeatherConditionCode.NOT_AVAILABLE.getConditionId();
            this.conditionImageId = WeatherConditionCode.NOT_AVAILABLE.getConditionImageId();
        } else {
            final WeatherConditionCode code = WeatherConditionCode.values()[forecast.getConditionCode()];
            this.conditionId = code.getConditionId();
            this.conditionImageId = code.getConditionImageId();
        }
        this.dayTemp = forecast.getDayTemp();
        this.nightTemp = forecast.getNightTemp();
        this.dayId = YahooDay.valueOf(forecast.getDayName()).getFullName();
    }

    @StringRes
    public int getConditionId() {
        return conditionId;
    }

    @DrawableRes
    public int getConditionImageId() {
        return conditionImageId;
    }

    public int getDayTemp() {
        return dayTemp;
    }

    public int getNightTemp() {
        return nightTemp;
    }

    @StringRes
    public int getDayId() {
        return dayId;
    }

    @Override
    public String toString() {
        return "OptimizedForecastItem{" +
                "conditionId=" + conditionId +
                ", conditionImageId=" + conditionImageId +
                ", dayTemp=" + dayTemp +
                ", nightTemp=" + nightTemp +
                ", dayId=" + dayId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedForecastItem)) return false;
        final OptimizedForecastItem that = (OptimizedForecastItem) o;
        return conditionId == that.conditionId &&
                conditionImageId == that.conditionImageId &&
                dayTemp == that.dayTemp &&
                nightTemp == that.nightTemp &&
                dayId == that.dayId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(conditionId, conditionImageId, dayTemp, nightTemp, dayId);
    }
}
