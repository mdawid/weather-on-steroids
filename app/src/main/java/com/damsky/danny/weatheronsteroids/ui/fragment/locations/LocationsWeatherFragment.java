package com.damsky.danny.weatheronsteroids.ui.fragment.locations;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.fragment.locations.adapter.LocationsRecyclerAdapter;
import com.damsky.danny.weatheronsteroids.work.DbOpsWorker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * The main fragment class. This class displays all locations and their current weather.
 * When using this fragment you must implement {@link Listener}.
 *
 * @author Danny Damsky
 * @see MainActivity
 */
public final class LocationsWeatherFragment extends Fragment {

    public static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.locations.LocationsWeatherFragment.TAG";

    /**
     * Puts the fragment inside the parent layout.
     *
     * @param fragmentManager the fragment manager that should hold the fragment.
     * @param parentLayoutId  the layout that will contain the fragment
     */
    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayoutId) {
        if (fragmentManager.findFragmentByTag(TAG) == null) {
            final LocationsWeatherFragment fragment = new LocationsWeatherFragment();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(parentLayoutId, fragment, TAG);
            transaction.commit();
        }
    }

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.emptyText)
    TextView emptyText;

    // Fab is only available on phones.
    @Nullable
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private LocationsRecyclerAdapter adapter;
    private MainActivity activity;
    private Unbinder unbinder;
    private WeatherLocationViewModel weatherLocationViewModel;
    private Listener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
        this.listener = (Listener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(activity)
                .inflate(R.layout.fragment_weathers_list, container, false);
        unbinder = ButterKnife.bind(this, layout);
        inflateLayout();
        updateAllPlacesIfEnabled();
        return layout;
    }

    private void inflateLayout() {
        configureRefreshLayoutColorScheme();
        configureEmptyText();
        setRefreshLayoutBehavior();
        setDefaultRecyclerViewProperties();
        setWeatherLocationsObserver();
    }

    private void configureRefreshLayoutColorScheme() {
        refreshLayout.setColorSchemeResources(R.color.colorSecondary, R.color.colorTertiary,
                R.color.colorSecondaryLight, R.color.colorTertiaryLight, R.color.colorSecondaryDark,
                R.color.colorTertiaryDark);
    }

    private void configureEmptyText() {
        final String first = getString(R.string.empty_text_locations_fragment_part_1)
                + "\n"
                + getString(R.string.empty_text_locations_fragment_part_2);
        final String second = " ";
        final String third = getString(R.string.empty_text_locations_fragment_part_3);

        final ImageSpan imageSpan;
        if (fab != null) {
            imageSpan = new ImageSpan(activity, R.drawable.ic_location_searching_secondary_black_24dp);
        } else {
            imageSpan = new ImageSpan(activity, R.drawable.ic_add_secondary_black_24dp);
        }

        final int lengthUntilImage = first.length() + second.length();
        final SpannableString string = new SpannableString(first + second + second + second + third);
        string.setSpan(imageSpan, lengthUntilImage, lengthUntilImage + second.length(), 0);
        emptyText.setText(string);
    }

    private void setRefreshLayoutBehavior() {
        refreshLayout.setOnRefreshListener(() -> DbOpsWorker.updateAllPlaces(activity));
    }

    private void setDefaultRecyclerViewProperties() {
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(null);
        setRecyclerViewAdapter();
    }

    private void setRecyclerViewAdapter() {
        adapter = new LocationsRecyclerAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void setWeatherLocationsObserver() {
        weatherLocationViewModel.getAllAdded().observe(this, optimizedLocations -> {
            configureVisibility(optimizedLocations);
            adapter.submitList(optimizedLocations);
            refreshLayout.setRefreshing(false);
        });
    }

    private void configureVisibility(@Nullable List<OptimizedLocationItem> optimizedLocations) {
        if (optimizedLocations == null || optimizedLocations.size() == 0) {
            hideOneViewAndShowTheOther(recyclerView, emptyText);
            refreshLayout.setEnabled(false);
        } else {
            hideOneViewAndShowTheOther(emptyText, recyclerView);
            if (!refreshLayout.isEnabled()) {
                refreshLayout.setEnabled(true);
            }
        }
    }

    private void hideOneViewAndShowTheOther(@NonNull View viewToHide, @NonNull View viewToShow) {
        fromVisibilityToVisibility(viewToHide, View.VISIBLE, View.INVISIBLE);
        fromVisibilityToVisibility(viewToShow, View.INVISIBLE, View.VISIBLE);
    }

    private void fromVisibilityToVisibility(@NonNull View view, int visibilityToCheck, int visibilityToSet) {
        if (view.getVisibility() == visibilityToCheck) {
            view.setVisibility(visibilityToSet);
        }
    }

    private void updateAllPlacesIfEnabled() {
        if (PreferencesHelper.getInstance().isRefreshWhenOpenedEnabled()) {
            DbOpsWorker.updateAllPlaces(activity);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Optional
    @OnClick(R.id.fab)
    public void onFabClicked() {
        listener.onFabClicked();
    }

    public interface Listener {
        void onFabClicked();
    }
}
