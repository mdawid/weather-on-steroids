package com.damsky.danny.weatheronsteroids.ui.fragment.dialog.alert;

import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * This class is meant to simplify passing data to {@link CustomizableDialogFragment}.
 * Use the {@link Builder} to build an instance of {@link DialogFragmentBundle} and pass it
 * to the show() function of {@link CustomizableDialogFragment} to show the dialog.
 *
 * @author Danny Damsky
 * @see CustomizableDialogFragment
 */
public final class DialogFragmentBundle {

    private static final String EXTRA_TITLE = "extra_title";
    private static final String EXTRA_MESSAGE = "extra_message";
    private static final String EXTRA_HINT = "extra_hint";
    private static final String EXTRA_TEXT = "extra_text";
    private static final String EXTRA_POSITIVE_TEXT = "extra_positive_text";
    private static final String EXTRA_NEGATIVE_TEXT = "extra_negative_text";
    private static final String EXTRA_FRAGMENT_TAG = "extra_fragment_tag";

    private final String title;
    private final String message;
    private final String hint;
    private final String text;
    private final String positiveText;
    private final String negativeText;
    private final String fragmentTag;

    private DialogFragmentBundle(@NonNull Builder builder) {
        this.title = builder.title;
        this.message = builder.message;
        this.hint = builder.hint;
        this.text = builder.text;
        this.positiveText = builder.positiveText;
        this.negativeText = builder.negativeText;
        this.fragmentTag = builder.fragmentTag;
    }

    DialogFragmentBundle(@NonNull Bundle bundle) {
        this.title = bundle.getString(EXTRA_TITLE);
        this.message = bundle.getString(EXTRA_MESSAGE);
        this.hint = bundle.getString(EXTRA_HINT);
        this.text = bundle.getString(EXTRA_TEXT);
        this.positiveText = bundle.getString(EXTRA_POSITIVE_TEXT);
        this.negativeText = bundle.getString(EXTRA_NEGATIVE_TEXT);
        this.fragmentTag = bundle.getString(EXTRA_FRAGMENT_TAG);
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getHint() {
        return hint;
    }

    public String getText() {
        return text;
    }

    public String getPositiveText() {
        return positiveText;
    }

    public String getNegativeText() {
        return negativeText;
    }

    public String getFragmentTag() {
        return fragmentTag;
    }

    @Override
    public String toString() {
        return "DialogFragmentBundle{" +
                "title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", hint='" + hint + '\'' +
                ", text='" + text + '\'' +
                ", positiveText='" + positiveText + '\'' +
                ", negativeText='" + negativeText + '\'' +
                ", fragmentTag='" + fragmentTag + '\'' +
                '}';
    }

    @NonNull
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TITLE, title);
        bundle.putString(EXTRA_MESSAGE, message);
        bundle.putString(EXTRA_HINT, hint);
        bundle.putString(EXTRA_TEXT, text);
        bundle.putString(EXTRA_POSITIVE_TEXT, positiveText);
        bundle.putString(EXTRA_NEGATIVE_TEXT, negativeText);
        bundle.putString(EXTRA_FRAGMENT_TAG, fragmentTag);
        return bundle;
    }

    public static final class Builder {
        private String title;
        private String message;
        private String hint;
        private String text;
        private String positiveText;
        private String negativeText;
        private String fragmentTag;

        public Builder setTitle(@NonNull String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(@NonNull String message) {
            this.message = message;
            return this;
        }

        public Builder setHint(@NonNull String hint) {
            this.hint = hint;
            return this;
        }

        public Builder setText(@NonNull String text) {
            this.text = text;
            return this;
        }

        public Builder setPositiveText(@NonNull String positiveText) {
            this.positiveText = positiveText;
            return this;
        }

        public Builder setNegativeText(@NonNull String negativeText) {
            this.negativeText = negativeText;
            return this;
        }

        public Builder setFragmentTag(String fragmentTag) {
            this.fragmentTag = fragmentTag;
            return this;
        }

        public DialogFragmentBundle build() {
            return new DialogFragmentBundle(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "title='" + title + '\'' +
                    ", message='" + message + '\'' +
                    ", hint='" + hint + '\'' +
                    ", text='" + text + '\'' +
                    ", positiveText='" + positiveText + '\'' +
                    ", negativeText='" + negativeText + '\'' +
                    ", fragmentTag='" + fragmentTag + '\'' +
                    '}';
        }
    }
}
