package com.damsky.danny.weatheronsteroids.data.weather.model;

import com.google.gson.annotations.SerializedName;

/**
 * This class is contained inside of {@link YahooItem} as an array.
 * The class holds weather information for a certain day of the week.
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 * @see YahooResults
 * @see YahooChannel
 * @see YahooItem
 */
public final class YahooForecast {

    @SerializedName("code")
    private final int conditionCode;
    @SerializedName("day")
    private final String dayName;
    @SerializedName("high")
    private final int dayTemp;
    @SerializedName("low")
    private final int nightTemp;

    public YahooForecast(int conditionCode, String dayName, int dayTemp, int nightTemp) {
        this.conditionCode = conditionCode;
        this.dayName = dayName;
        this.dayTemp = dayTemp;
        this.nightTemp = nightTemp;
    }

    public int getConditionCode() {
        return conditionCode;
    }

    public String getDayName() {
        return dayName;
    }

    public int getDayTemp() {
        return dayTemp;
    }

    public int getNightTemp() {
        return nightTemp;
    }

    @Override
    public String toString() {
        return "YahooForecast{" +
                "conditionCode=" + conditionCode +
                ", dayName='" + dayName + '\'' +
                ", dayTemp=" + dayTemp +
                ", nightTemp=" + nightTemp +
                '}';
    }
}
