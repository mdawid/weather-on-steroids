package com.damsky.danny.weatheronsteroids.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;

/**
 * Helper class for work with SharedPreferences throughout the application.
 * The class uses the Singleton design pattern to ensure that there's only one instance of
 * Shared Preferences in the entire application.
 *
 * @author Danny Damsky
 */
public final class PreferencesHelper {

    public static final String KEY_NIGHT_MODE = "key_night_mode";
    public static final String KEY_REFRESH_WHEN_OPENED = "key_refresh";
    public static final String KEY_AUTO_UPDATE = "key_auto_update";
    public static final String KEY_UPDATE_SCHEDULE = "key_update_schedule";
    public static final String KEY_SYSTEM_OF_MEASUREMENT = "key_system_of_measurement";
    public static final String KEY_CLEAR_DATA = "key_clear_data";

    private static final String VALUE_SYSTEM_OF_MEASUREMENT_METRIC = "metric";
    private static final String VALUE_SYSTEM_OF_MEASUREMENT_IMPERIAL = "imperial";

    public static final int VALUE_UPDATE_SCHEDULE_ONE_HOUR = 1;
    public static final int VALUE_UPDATE_SCHEDULE_TWO_HOURS = 2;
    public static final int VALUE_UPDATE_SCHEDULE_THREE_HOURS = 3;
    public static final int VALUE_UPDATE_SCHEDULE_SIX_HOURS = 6;
    public static final int VALUE_UPDATE_SCHEDULE_TWELVE_HOURS = 12;
    public static final int VALUE_UPDATE_SCHEDULE_ONE_DAY = 24;

    private static PreferencesHelper instance;

    @NonNull
    public static PreferencesHelper getInstance() {
        return instance;
    }

    public static void initialize(@NonNull Context context) {
        instance = new PreferencesHelper(context);
    }

    private final SharedPreferences defaultSharedPreferences;

    private PreferencesHelper(@NonNull Context context) {
        defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isMetricUnits() {
        final String systemOfMeasurement = defaultSharedPreferences
                .getString(KEY_SYSTEM_OF_MEASUREMENT, VALUE_SYSTEM_OF_MEASUREMENT_METRIC);
        return systemOfMeasurement.equals(VALUE_SYSTEM_OF_MEASUREMENT_METRIC);
    }

    public int getNightMode() {
        if (isNightModeEnabled()) {
            return AppCompatDelegate.MODE_NIGHT_YES;
        }
        return AppCompatDelegate.MODE_NIGHT_NO;
    }

    public boolean isNightModeEnabled() {
        return defaultSharedPreferences.getBoolean(KEY_NIGHT_MODE, false);
    }

    public boolean isAutoUpdateEnabled() {
        return defaultSharedPreferences.getBoolean(KEY_AUTO_UPDATE, true);
    }

    public boolean isRefreshWhenOpenedEnabled() {
        return defaultSharedPreferences.getBoolean(KEY_REFRESH_WHEN_OPENED, false);
    }

    public int getUpdateScheduleInHours() {
        return Integer.parseInt(defaultSharedPreferences.getString(KEY_UPDATE_SCHEDULE, "1"));
    }

    public SharedPreferences getDefaultSharedPreferences() {
        return defaultSharedPreferences;
    }
}
