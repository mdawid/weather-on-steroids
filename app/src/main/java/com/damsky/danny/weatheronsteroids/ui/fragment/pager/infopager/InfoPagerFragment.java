package com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.adapter.InfoPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * This fragment holds {@link com.damsky.danny.weatheronsteroids.ui.fragment.about.AboutFragment}
 * and {@link com.damsky.danny.weatheronsteroids.ui.fragment.prefs.PreferencesFragment}
 * in a Tabbed ViewPager.
 *
 * @author Danny Damsky
 */
public final class InfoPagerFragment extends Fragment {

    public static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.InfoPagerFragment.TAG";

    /**
     * Shows the fragment inside the parent layout.
     *
     * @param fragmentManager the fragment manager to hold the fragment.
     * @param parentLayoutId  the layout to contain the fragment.
     */
    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayoutId) {
        if (fragmentManager.findFragmentByTag(TAG) == null) {
            final InfoPagerFragment fragment = new InfoPagerFragment();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(parentLayoutId, fragment, TAG);
            transaction.commit();
        }
    }

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    private MainActivity activity;
    private PagerAdapter pagerAdapter;
    private Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(activity)
                .inflate(R.layout.fragment_pager, container, false);
        unbinder = ButterKnife.bind(this, layout);
        setupPager();
        return layout;
    }

    private void setupPager() {
        pagerAdapter = new InfoPagerAdapter(this);
        pager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(pager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
