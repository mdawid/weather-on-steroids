package com.damsky.danny.weatheronsteroids.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhoto;
import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhotoDao;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocationDao;

/**
 * @author Danny Damsky
 * @see RoomDatabase
 */
@Database(entities = {WeatherLocation.class, LocationPhoto.class}, version = 1, exportSchema = false)
public abstract class WeatherLocationRoomDatabase extends RoomDatabase {

    public abstract WeatherLocationDao weatherLocationDao();

    public abstract LocationPhotoDao locationPhotoDao();

    private static WeatherLocationRoomDatabase INSTANCE;
    private static final String DB_NAME = "weatherinfo.db";

    public static WeatherLocationRoomDatabase getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            synchronized (WeatherLocationRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            WeatherLocationRoomDatabase.class, DB_NAME)
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
