package com.damsky.danny.weatheronsteroids.ui.fragment.litemap;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.maps.MapClusterItem;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A lite version of the {@link SupportMapFragment} for use inside the
 * {@link com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.WeatherViewPagerFragment}.
 *
 * @author Danny Damsky
 */
public final class LiteMapFragment extends SupportMapFragment implements OnMapReadyCallback {

    private static GoogleMapOptions getOptions() {
        return new GoogleMapOptions()
                .liteMode(true)
                .compassEnabled(true)
                .rotateGesturesEnabled(true)
                .scrollGesturesEnabled(true)
                .tiltGesturesEnabled(true)
                .mapToolbarEnabled(false);
    }

    /**
     * Overrides its parent class' newInstance() function to provide custom map options when
     * initializing this class.
     *
     * @return a {@link LiteMapFragment} object.
     * @see SupportMapFragment#newInstance(GoogleMapOptions)
     */
    public static LiteMapFragment newInstance(@NonNull Bundle args) {
        args.putParcelable("MapOptions", getOptions());
        final LiteMapFragment fragment = new LiteMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Context context;
    private GoogleMap map;
    private WeatherLocationViewModel weatherLocationViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onStart() {
        super.onStart();
        weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
        getMapAsync(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setClickable(false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        setMapProperties();
        initializeZoom();
    }

    private void setMapProperties() {
        if (PreferencesHelper.getInstance().isNightModeEnabled()) {
            map.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.maps_night_mode));
        }
    }

    private void initializeZoom() {
        final Bundle bundle = getArguments();
        final String itemId = bundle.getString(Constants.EXTRA_ID);
        weatherLocationViewModel.getById(itemId).observe(this, location -> {
            zoomToPlace(new LatLng(location.getLatitude(), location.getLongitude()));
            weatherLocationViewModel.getById(itemId).removeObservers(this);
            setLocationMarkersObserver(itemId);
        });
    }

    private void zoomToPlace(@NonNull LatLng coordinates) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15.0f));
    }

    private void setLocationMarkersObserver(final String itemId) {
        final LiveData<MapClusterItem> liveData = weatherLocationViewModel.getMarkerById(itemId);
        liveData.observe(this, mapClusterItem -> {
            map.addMarker(new MarkerOptions()
                    .position(mapClusterItem.getPosition()));
            liveData.removeObservers(this);
        });
    }
}
