package com.damsky.danny.weatheronsteroids.data.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.damsky.danny.weatheronsteroids.util.ImageUtils;

import java.util.Objects;

/**
 * @author Danny Damsky
 * @see Entity
 */
@Entity(tableName = "photos",
        foreignKeys = @ForeignKey(entity = WeatherLocation.class,
                parentColumns = "id",
                childColumns = "parent_id",
                onDelete = ForeignKey.CASCADE),
        indices = {@Index("parent_id"), @Index(value = "bitmap", unique = true)})
public final class LocationPhoto {

    public static final DiffUtil.ItemCallback<LocationPhoto> DIFF_CALLBACK = new DiffUtil.ItemCallback<LocationPhoto>() {
        @Override
        public boolean areItemsTheSame(@NonNull LocationPhoto locationPhoto, @NonNull LocationPhoto t1) {
            return locationPhoto.id == t1.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull LocationPhoto locationPhoto, @NonNull LocationPhoto t1) {
            return locationPhoto.equals(t1);
        }
    };

    @PrimaryKey(autoGenerate = true)
    private long id;
    @NonNull
    @ColumnInfo(name = "parent_id")
    private String parentId;
    @NonNull
    @ColumnInfo(name = "bitmap")
    private String bitmap;

    @Ignore
    private LocationPhoto(@NonNull Builder builder) {
        this.parentId = builder.parentId;
        this.bitmap = builder.bitmap;
    }

    public LocationPhoto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @NonNull
    public String getParentId() {
        return parentId;
    }

    public void setParentId(@NonNull String parentId) {
        this.parentId = parentId;
    }

    @NonNull
    public String getBitmap() {
        return bitmap;
    }

    public void setBitmap(@NonNull String bitmap) {
        this.bitmap = bitmap;
    }

    @Override
    public String toString() {
        return "LocationPhoto{" +
                "id=" + id +
                ", parentId='" + parentId + '\'' +
                ", bitmap='" + bitmap + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationPhoto)) return false;
        final LocationPhoto photo = (LocationPhoto) o;
        return id == photo.id &&
                Objects.equals(parentId, photo.parentId) &&
                Objects.equals(bitmap, photo.bitmap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, parentId, bitmap);
    }

    public static final class Builder {
        private String parentId;
        private String bitmap;

        @NonNull
        public Builder setParentId(@NonNull String parentId) {
            this.parentId = parentId;
            return this;
        }

        @NonNull
        public Builder setBitmap(@NonNull Bitmap bitmap) {
            this.bitmap = ImageUtils.bitmapToString(bitmap);
            return this;
        }

        @NonNull
        public LocationPhoto build() {
            return new LocationPhoto(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "parentId='" + parentId + '\'' +
                    ", bitmap='" + bitmap + '\'' +
                    '}';
        }
    }
}
