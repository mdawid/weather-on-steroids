package com.damsky.danny.weatheronsteroids.customtabs;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;

/**
 * @author Sascha Peilicke
 * @see <a href="https://github.com/saschpe/android-customtabs">Android CustomTabs Library</a>
 */
public final class CustomTabsHelper {

    private static final String EXTRA_CUSTOM_TABS_KEEP_ALIVE = "android.support.customtabs.extra.KEEP_ALIVE";

    private CustomTabsSession customTabsSession;
    private CustomTabsClient client;
    private CustomTabsServiceConnection connection;

    CustomTabsHelper() {
    }

    /**
     * Opens the URL on a Custom Tab if possible. Otherwise falls back to opening it on a WebView
     *
     * @param context          the host activity
     * @param customTabsIntent a CustomTabsIntent to be used if Custom Tabs is available
     * @param uri              the Uri to be opened
     * @param fallback         a CustomTabFallback to be used if Custom Tabs is not available
     */
    public static void openCustomTab(@NonNull final Context context,
                                     @NonNull final CustomTabsIntent customTabsIntent,
                                     @NonNull final Uri uri,
                                     @NonNull final CustomTabFallback fallback) {
        final String packageName = CustomTabsPackageHelper.getPackageNameToUse(context);
        if (packageName == null) {
            fallback.openUri(context, uri);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                customTabsIntent.intent.putExtra(Intent.EXTRA_REFERRER,
                        Uri.parse(Intent.URI_ANDROID_APP_SCHEME + "//" + context.getPackageName()));
            }
            customTabsIntent.intent.setPackage(packageName);
            customTabsIntent.launchUrl(context, uri);
        }
    }

    public static void addKeepAliveExtra(@NonNull final Context context, @NonNull final Intent intent) {
        final Intent keepAliveIntent = new Intent().setClassName(
                context.getPackageName(), KeepAliveService.class.getCanonicalName());
        intent.putExtra(EXTRA_CUSTOM_TABS_KEEP_ALIVE, keepAliveIntent);
    }

    public void unbindCustomTabsService(@NonNull final Activity activity) {
        if (connection != null) {
            activity.unbindService(connection);
            client = null;
            customTabsSession = null;
        }
    }

    private void createOrNullifySession() {
        if (client == null) {
            customTabsSession = null;
        } else if (customTabsSession == null) {
            customTabsSession = client.newSession(null);
        }
    }

    /**
     * Binds the Activity to the Custom Tabs Service
     *
     * @param activity the activity to be bound to the service
     */
    public void bindCustomTabsService(final Activity activity) {
        if (client == null) {
            final String packageName = CustomTabsPackageHelper.getPackageNameToUse(activity);
            if (packageName != null) {
                connection = new CustomTabsServiceConnection() {
                    @Override
                    public void onCustomTabsServiceConnected(ComponentName name, CustomTabsClient client) {
                        CustomTabsHelper.this.client = client;
                        CustomTabsHelper.this.client.warmup(0L);
                        createOrNullifySession();
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {
                        client = null;
                    }

                    @Override
                    public void onBindingDied(ComponentName name) {
                        client = null;
                    }
                };
                CustomTabsClient.bindCustomTabsService(activity, packageName, connection);
            }
        }
    }

    /**
     * To be used as a fallback to open the Uri when Custom Tabs is not available
     */
    public interface CustomTabFallback {
        /**
         * @param context The Activity that wants to open the Uri
         * @param uri     The uri to be opened by the fallback
         */
        void openUri(Context context, Uri uri);
    }
}
