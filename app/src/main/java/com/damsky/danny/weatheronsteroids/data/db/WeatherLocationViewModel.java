package com.damsky.danny.weatheronsteroids.data.db;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhoto;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.maps.MapClusterItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the ViewModel that's being used in the application's activities and fragments.
 * It provides processed location data including: weather, map markers, photos.
 * It also provides DB operation functions.
 *
 * @see AndroidViewModel
 * @see WeatherLocationRepository
 */
public final class WeatherLocationViewModel extends AndroidViewModel {

    private final WeatherLocationRepository weatherLocationRepository;
    private final LiveData<List<OptimizedLocationItem>> optimizedLocations;
    private final LiveData<List<MapClusterItem>> locationMarkers;

    public WeatherLocationViewModel(@NonNull Application application) {
        super(application);
        weatherLocationRepository = WeatherLocationRepository.getInstance(application);
        final LiveData<List<WeatherLocation>> addedWeatherLocations = weatherLocationRepository.getAllAdded();
        optimizedLocations = Transformations.switchMap(addedWeatherLocations, this::getAddedOptimizedLocationsLiveData);
        locationMarkers = Transformations.switchMap(optimizedLocations, this::getLocationMarkersLiveData);
    }

    private LiveData<List<OptimizedLocationItem>> getAddedOptimizedLocationsLiveData(@NonNull List<WeatherLocation> input) {
        final List<OptimizedLocationItem> optimizedLocationItems = iterateOverWeatherLocations(input);
        final MutableLiveData<List<OptimizedLocationItem>> liveData = new MutableLiveData<>();
        liveData.setValue(optimizedLocationItems);
        return liveData;
    }

    @NonNull
    private List<OptimizedLocationItem> iterateOverWeatherLocations(@NonNull List<WeatherLocation> input) {
        final int length = input.size();
        final List<OptimizedLocationItem> optimizedLocationItems = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            optimizedLocationItems.add(new OptimizedLocationItem(input.get(i)));
        }
        return optimizedLocationItems;
    }

    @NonNull
    private List<MapClusterItem> iterateOverOptimizedLocations(@NonNull List<OptimizedLocationItem> input) {
        final int length = input.size();
        final List<MapClusterItem> mapClusterItems = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            mapClusterItems.add(new MapClusterItem(input.get(i)));
        }
        return mapClusterItems;
    }

    @NonNull
    private LiveData<List<MapClusterItem>> getLocationMarkersLiveData(@NonNull List<OptimizedLocationItem> input) {
        final List<MapClusterItem> mapClusterItems = iterateOverOptimizedLocations(input);
        final MutableLiveData<List<MapClusterItem>> liveData = new MutableLiveData<>();
        liveData.setValue(mapClusterItems);
        return liveData;
    }

    /**
     * @return {@link LiveData} of all {@link WeatherLocation} objects where isAdded = true
     */
    @NonNull
    public LiveData<List<OptimizedLocationItem>> getAllAdded() {
        return optimizedLocations;
    }

    /**
     * @param id the ID of the location.
     * @return a {@link LiveData} object of the location.
     */
    @NonNull
    public LiveData<WeatherLocation> getById(@NonNull String id) {
        return weatherLocationRepository.getById(id);
    }

    /**
     * @param latitude  the latitude of the location
     * @param longitude the longitude of the location
     * @return {@link LiveData} object of the location.
     */
    @NonNull
    private LiveData<WeatherLocation> getByLocation(double latitude, double longitude) {
        return weatherLocationRepository.getByLocation(latitude, longitude);
    }

    /**
     * @param id the ID of the location.
     * @return a {@link LiveData} object of the location with its data post-processed.
     * @see OptimizedLocationItem
     */
    @NonNull
    public LiveData<OptimizedLocationItem> getOptimizedById(@NonNull String id) {
        final LiveData<WeatherLocation> weatherLocationLiveData = weatherLocationRepository.getById(id);
        return Transformations.switchMap(weatherLocationLiveData, this::getOptimizedLocationLiveData);
    }

    /**
     * @param latLng the coordinates of the location
     * @return a {@link LiveData} object of the location with its data post-processed.
     * @see OptimizedLocationItem
     */
    @NonNull
    public LiveData<OptimizedLocationItem> getOptimizedByLocation(@NonNull LatLng latLng) {
        final LiveData<WeatherLocation> liveData = getByLocation(latLng.latitude, latLng.longitude);
        return Transformations.switchMap(liveData, this::getOptimizedLocationLiveData);
    }

    @NonNull
    private LiveData<OptimizedLocationItem> getOptimizedLocationLiveData(@Nullable WeatherLocation weatherLocation) {
        OptimizedLocationItem item = null;
        if (weatherLocation != null) {
            item = new OptimizedLocationItem(weatherLocation);
        }
        final MutableLiveData<OptimizedLocationItem> liveData = new MutableLiveData<>();
        liveData.setValue(item);
        return liveData;
    }

    /**
     * @param id the ID of the location.
     * @return a {@link LiveData} object of the location's marker on the map.
     * @see MapClusterItem
     */
    @NonNull
    public LiveData<MapClusterItem> getMarkerById(@NonNull String id) {
        final LiveData<OptimizedLocationItem> optimizedLocationItemLiveData = getOptimizedById(id);
        return Transformations.switchMap(optimizedLocationItemLiveData, this::getMarkerLiveData);
    }

    @NonNull
    private LiveData<MapClusterItem> getMarkerLiveData(@NonNull OptimizedLocationItem optimizedLocationItem) {
        final MapClusterItem item = new MapClusterItem(optimizedLocationItem);
        final MutableLiveData<MapClusterItem> liveData = new MutableLiveData<>();
        liveData.setValue(item);
        return liveData;
    }

    /**
     * @return a {@link LiveData} of all location markers.
     * @see MapClusterItem
     */
    @NonNull
    public LiveData<List<MapClusterItem>> getLocationMarkers() {
        return locationMarkers;
    }

    /**
     * Updates the location in the database asynchronously.
     * If the user is logged in, it also updates the location in Firebase Firestore.
     *
     * @param location the location to update in the database.
     */
    public void update(@NonNull WeatherLocation location) {
        weatherLocationRepository.update(location);
    }

    /**
     * Removes the location from user visibility by setting its isAdded property to false.
     * If the user is logged in, it also removes the locations in Firebase Firestore.
     *
     * @param location the location to remove.
     */
    public void delete(@NonNull WeatherLocation location) {
        weatherLocationRepository.delete(location);
    }

    /**
     * Removes the location from user visibility by setting its isAdded property to false.
     * If the user is logged in, it also removes the locations in Firebase Firestore.
     *
     * @param id the ID of the location to remove.
     */
    public void delete(@NonNull String id) {
        weatherLocationRepository.deleteLocation(id);
    }

    /**
     * Removes all locations from user visibility by setting their isAdded property to false.
     * If the user is logged in, the function also removes the locations in Firebase Firestore.
     */
    public void deleteAll() {
        weatherLocationRepository.deleteAll();
    }

    /**
     * Restores the location by setting its isAdded property to true.
     * If the user is logged in, it also restores the locations in Firebase Firestore.
     *
     * @param id the ID of the location to restore.
     */
    public void restore(@NonNull String id) {
        weatherLocationRepository.restoreLocation(id);
    }

    /**
     * @param placeId the id of the location.
     * @return a {@link LiveData} object of a {@link PagedList} of the location's photos.
     * @see PagedList
     */
    @NonNull
    public LiveData<PagedList<LocationPhoto>> getPagedLocationPhotos(@NonNull String placeId) {
        return new LivePagedListBuilder<>(
                weatherLocationRepository.getLocationPhotosDataSource(placeId),
                new PagedList.Config.Builder()
                        .setPageSize(5)
                        .setPrefetchDistance(10)
                        .setEnablePlaceholders(true)
                        .build()).build();
    }
}
